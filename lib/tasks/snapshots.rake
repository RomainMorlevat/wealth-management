namespace :snapshots do
  desc "Create snapshots from transactions"
  task create_from_transactions: :environment do
    Account.all.each do |account|
      balance = 0
      account.transactions.order(date: :asc).each do |transaction|
        balance += convert_amount(transaction)
        Snapshot.create(account: account,
          balance: balance,
          created_at: transaction.date)
      end
    end
  end

  private

  def convert_amount(transaction)
    transaction.debit? ? -transaction.amount : transaction.amount
  end
end
