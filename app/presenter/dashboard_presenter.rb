class DashboardPresenter
  def account_stats
    if Account.count.zero?
      {
        "No Data" => 0
      }
    else
      tot_balance = total_balance
      Account.all
        .group_by(&:category)
        .transform_values do |accounts|
          calculate_percentage(accounts.map(&:balance_cents).sum, tot_balance)
        end
    end
  end

  def balances_of_accounts_over_time
    result = {"datasets" => [], "labels" => []}
    return result if Account.count.zero? || Snapshot.count.zero?

    Account.all.each do |account|
      snapshots = account.snapshots.order(created_at: :asc)
      datas = snapshots.map { |snapshot|
        label = snapshot.created_on
        result["labels"] << label
        {
          x: label,
          y: snapshot.balance_cents.to_f / 100
        }
      }

      result["datasets"] << {
        "label" => account.name,
        "data" => datas
      }
    end

    result["labels"] = result["labels"].uniq.sort

    result
  end

  def total_balance
    Snapshot.select('DISTINCT ON ("account_id") *')
      .order(:account_id, created_at: :desc)
      .map(&:balance_cents)
      .sum
      .to_f / 100
  end

  def total_balance_over_time
    query_result = ActiveRecord::Base.connection.execute(total_balance_over_time_query).to_a

    data = query_result.group_by { |row| row["year"] }.transform_values do |rows|
      rows.map { |row| {x: row["month"], y: row["monthly_balance"]} }
    end

    datasets = data.map do |year, points|
      {
        label: "Year #{year.to_i}",
        data: points,
        fill: false,
        borderColor: "##{Digest::MD5.hexdigest(year.to_s)[0..5]}",
        tension: 0.1
      }
    end

    {
      labels: query_result.map { |row| row["month"] },
      datasets: datasets
    }
  end

  def total_wealth
    total_balance + real_estate_total_valuation_cents.to_f / 100
  end

  def wealth_stats
    tot_wealth = total_wealth
    result = Account.all
      .group_by(&:category)
      .transform_values do |accounts|
        calculate_percentage(accounts.map(&:balance_cents).sum, tot_wealth)
      end
    result["realEstate"] = calculate_percentage(real_estate_total_valuation_cents, tot_wealth)
    result
  end

  def ytd
    query_result = ActiveRecord::Base.connection.execute(total_balance_over_time_query).to_a
    query_result.group_by { |ytd| ytd["year"] }.map do |year, rows|
      sorted_rows = rows.sort_by { |row| row["month"] }
      first_month_balance = sorted_rows.first["monthly_balance"].to_f
      last_month_balance = sorted_rows.last["monthly_balance"].to_f

      ytd_percentage = if first_month_balance.to_f.zero?
        next
      else
        ((last_month_balance - first_month_balance) / first_month_balance).round(4)
      end

      {
        year: year.to_i,
        ytd_percentage: ytd_percentage
      }
    end.compact
  end

  private

  def calculate_percentage(number, total)
    (number / total).round(2)
  end

  def real_estate_total_valuation_cents
    RealEstate.includes(:valuations).all.map(&:current_valuation).sum
  end

  def total_balance_over_time_query(to_date = Date.current.iso8601)
    <<~SQL
      WITH date_range AS (
        SELECT MIN(DATE_TRUNC('month', created_at)) AS start_date,
               MAX(DATE_TRUNC('month', created_at)) AS end_date
        FROM snapshots
      ),
      months AS (
        SELECT generate_series(start_date, GREATEST(end_date, DATE_TRUNC('month', DATE('#{to_date}'))), '1 month'::interval) AS month
        FROM date_range
      ),
      account_months AS (
        SELECT a.id AS account_id, m.month
        FROM accounts a
        CROSS JOIN months m
      ),
      monthly_balances AS (
        SELECT 
          am.account_id,
          am.month,
          COALESCE(
            (SELECT balance_cents
             FROM snapshots s
             WHERE s.account_id = am.account_id
               AND s.created_at <= am.month
             ORDER BY s.created_at DESC
             LIMIT 1),
            0
          ) AS balance_cents
        FROM account_months am
      ),
      monthly_totals AS (
        SELECT 
          TO_CHAR(month, 'YYYY-MM-DD') AS month,
          DATE_PART('year', month) AS year,
          SUM(balance_cents) / 100 AS monthly_balance
        FROM monthly_balances
        GROUP BY month
      ),
      current_total AS (
        SELECT 
          TO_CHAR(DATE('#{to_date}'), 'YYYY-MM-DD') AS month,
          DATE_PART('year', DATE('#{to_date}')) AS year,
          SUM(COALESCE(
            (SELECT balance_cents
             FROM snapshots s
             WHERE s.account_id = a.id
               AND s.created_at <= DATE('#{to_date}')
             ORDER BY s.created_at DESC
             LIMIT 1),
            0
          )) / 100 AS monthly_balance
        FROM accounts a
      )
      SELECT * FROM monthly_totals
      UNION ALL
      SELECT * FROM current_total
      ORDER BY month;
    SQL
  end
end
