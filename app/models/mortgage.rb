class Mortgage < ApplicationRecord
  belongs_to :real_estate

  validates :loan_amount_cents, presence: true, numericality: {greater_than_or_equal_to: 0}
  validates :loan_amount_currency, presence: true
  validates :interest_rate, presence: true, numericality: {greater_than_or_equal_to: 0, less_than: 100}
  validates :term_years, presence: true, numericality: {greater_than: 0}
  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :monthly_payment_cents, presence: true, numericality: {greater_than_or_equal_to: 0}
  validates :monthly_payment_currency, presence: true

  validate :end_date_after_start_date

  monetize :loan_amount_cents
  monetize :monthly_payment_cents

  def amount_repaid
    months_elapsed = (Time.zone.today - start_date).to_i / 30
    return 0 unless months_elapsed.positive?

    monthly_payment_cents * months_elapsed
  end

  def amount_remaining
    loan_amount_cents - amount_repaid
  end

  private

  def end_date_after_start_date
    return if end_date.blank? || start_date.blank?

    if end_date <= start_date
      errors.add(:end_date, "must be after the start date")
    end
  end
end
