class Underlying < ApplicationRecord
  validates :name, presence: true
  validates :isin, uniqueness: true

  enum kind: {
    stocks: 0,
    bonds: 1
  }
end
