class Account < ApplicationRecord
  has_many :snapshots, dependent: :destroy

  validates :bank_name, presence: true
  validates :category, presence: true
  validates :name, presence: true, uniqueness: {scope: :bank_name}

  enum category: {
    current: 0,
    saving: 1,
    stocks: 2
  }

  def balance_cents
    snapshots.order(created_at: :desc).first&.balance_cents || 0
  end
end
