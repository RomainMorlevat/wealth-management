class RealEstate < ApplicationRecord
  has_many :mortgages
  has_many :valuations

  validates :name, presence: true
  validates :purchase_price_cents, presence: true

  monetize :purchase_price_cents

  def current_valuation
    valuations.order(date: :desc).first&.price_cents || purchase_price_cents
  end

  def total_amount_repaid
    mortgages.sum(&:amount_repaid)
  end

  def total_amount_remaining
    mortgages.sum(&:amount_remaining)
  end
end
