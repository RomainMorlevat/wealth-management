class Snapshot < ApplicationRecord
  belongs_to :account

  validates :balance_cents, presence: true

  monetize :balance_cents

  def created_on
    created_at.strftime("%Y-%m-%d")
  end
end
