class Valuation < ApplicationRecord
  belongs_to :real_estate

  validates :price_cents, presence: true
  validates :date, presence: true

  monetize :price_cents

  # Ensure that there's only one valuation per date for each real estate
  validates :date, uniqueness: {scope: :real_estate_id}
end
