import React from 'react'
import ReactDOM from 'react-dom'
import App from '../components/App'

import '../css/application.css'

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <App />,
    document.getElementById('root')
  )
})
