import React, { useState } from 'react'

const MortgageForm = ({ httpAction, onCancel }) => {
  const [formData, setFormData] = useState({
    loanAmountCents: '',
    interestRate: '',
    termYears: '',
    startDate: '',
    endDate: '',
    monthlyPaymentCents: ''
  })

  const handleChange = (e) => {
    const { name, value } = e.target
    setFormData({ ...formData, [name]: value })
  }

  const camelToSnakeCase = (str) => str.replace(/[A-Z]/g, (letter) => `_${letter.toLowerCase()}`)

  const convertToSnakeCase = (obj) => {
    return Object.keys(obj).reduce((acc, key) => {
      acc[camelToSnakeCase(key)] = obj[key]
      return acc
    }, {})
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    const snakeCaseData = convertToSnakeCase(formData)
    const payload = {
      mortgage: {
        ...snakeCaseData,
        loan_amount_cents: Math.round(parseFloat(snakeCaseData.loan_amount_cents) * 100),
        monthly_payment_cents: Math.round(parseFloat(snakeCaseData.monthly_payment_cents) * 100),
        interest_rate: parseFloat(snakeCaseData.interest_rate),
        term_years: parseInt(snakeCaseData.term_years)
      }
    }
    httpAction(payload)
  }

  return (
    <form onSubmit={handleSubmit} className='bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4'>
      <div className='mb-4'>
        <label className='block text-gray-700 text-sm font-bold mb-2' htmlFor='loanAmountCents'>
          Loan Amount
        </label>
        <input
          className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
          id='loanAmountCents'
          type='number'
          step='0.01'
          name='loanAmountCents'
          value={formData.loanAmountCents}
          onChange={handleChange}
          required
        />
      </div>
      <div className='mb-4'>
        <label className='block text-gray-700 text-sm font-bold mb-2' htmlFor='interestRate'>
          Interest Rate (%)
        </label>
        <input
          className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
          id='interestRate'
          type='number'
          step='0.01'
          name='interestRate'
          value={formData.interestRate}
          onChange={handleChange}
          required
        />
      </div>
      <div className='mb-4'>
        <label className='block text-gray-700 text-sm font-bold mb-2' htmlFor='termYears'>
          Term (Years)
        </label>
        <input
          className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
          id='termYears'
          type='number'
          name='termYears'
          value={formData.termYears}
          onChange={handleChange}
          required
        />
      </div>
      <div className='mb-4'>
        <label className='block text-gray-700 text-sm font-bold mb-2' htmlFor='startDate'>
          Start Date
        </label>
        <input
          className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
          id='startDate'
          type='date'
          name='startDate'
          value={formData.startDate}
          onChange={handleChange}
          required
        />
      </div>
      <div className='mb-4'>
        <label className='block text-gray-700 text-sm font-bold mb-2' htmlFor='endDate'>
          End Date
        </label>
        <input
          className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
          id='endDate'
          type='date'
          name='endDate'
          value={formData.endDate}
          onChange={handleChange}
          required
        />
      </div>
      <div className='mb-4'>
        <label className='block text-gray-700 text-sm font-bold mb-2' htmlFor='monthlyPaymentCents'>
          Monthly Payment
        </label>
        <input
          className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
          id='monthlyPaymentCents'
          type='number'
          step='0.01'
          name='monthlyPaymentCents'
          value={formData.monthlyPaymentCents}
          onChange={handleChange}
          required
        />
      </div>
      <div className='flex items-center justify-between'>
        <button
          className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline'
          type='submit'
        >
          Create Mortgage
        </button>
        <button
          className='bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline'
          type='button'
          onClick={onCancel}
        >
          Cancel
        </button>
      </div>
    </form>
  )
}

export default MortgageForm
