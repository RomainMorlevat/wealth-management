import React from 'react'

const MorgagesTable = (realEstate) => (
  <table className='w-full mt-4 bg-white shadow-md rounded'>
    <thead>
      <tr className='bg-gray-200 text-gray-600 uppercase text-sm leading-normal'>
        <th className='py-3 px-6 text-left'>Loan Amount</th>
        <th className='py-3 px-6 text-left'>Interest Rate</th>
        <th className='py-3 px-6 text-left'>Term</th>
        <th className='py-3 px-6 text-left'>Amount Repaid</th>
        <th className='py-3 px-6 text-left'>Amount Remaining</th>
      </tr>
    </thead>
    <tbody className='text-gray-600 text-sm font-light'>
      {realEstate.mortgages.map((mortgage, index) => (
        <tr key={index} className='border-b border-gray-200 hover:bg-gray-100'>
          <td className='py-3 px-6 text-left whitespace-no-wrap'>
            {(mortgage.loanAmountCents / 100).toLocaleString('fr-FR', {
              style: 'currency',
              currency: 'EUR'
            })}
          </td>
          <td className='py-3 px-6 text-left'>{mortgage.interestRate}%</td>
          <td className='py-3 px-6 text-left'>{mortgage.termYears} years</td>
          <td className='py-3 px-6 text-left'>
            {(mortgage.amountRepaid / 100).toLocaleString('fr-FR', {
              style: 'currency',
              currency: 'EUR'
            })}
          </td>
          <td className='py-3 px-6 text-left'>
            {(mortgage.amountRemaining / 100).toLocaleString('fr-FR', {
              style: 'currency',
              currency: 'EUR'
            })}
          </td>
        </tr>
      ))}
      <tr key='total' className='border-b border-gray-200 hover:bg-gray-100'>
        <td />
        <td />
        <td className='py-3 px-6'>Total</td>
        <td className='py-3 px-6 text-left'>
          {(realEstate.totalAmountRepaid / 100).toLocaleString('fr-FR', {
            style: 'currency',
            currency: 'EUR'
          })}
        </td>
        <td className='py-3 px-6 text-left'>
          {(realEstate.totalAmountRemaining / 100).toLocaleString('fr-FR', {
            style: 'currency',
            currency: 'EUR'
          })}
        </td>
      </tr>
    </tbody>
  </table>
)

export default MorgagesTable
