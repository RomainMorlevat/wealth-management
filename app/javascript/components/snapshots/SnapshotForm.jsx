import React, { useState } from 'react'

const SnapshotForm = ({ disactivate, httpAction, snapshot = {} }) => {
  const [balance, setBalance] = useState(snapshot.balanceCents / 100 || '')
  const [required, setRequired] = useState(false)

  const isSnapshotValid = () => balance

  const handleSubmit = (event) => {
    event.preventDefault()

    if (isSnapshotValid()) {
      const payload = {
        balance_cents: balance * 100,
        snapshot_id: snapshot.id
      }

      httpAction(payload)
      disactivate()
    } else {
      setRequired(true)
    }
  }
  return (
    <>
      <tr>
        <td className='px-6 py-4 whitespace-no-wrap border-b border-gray-200'>
          <input
            id='balance'
            className='appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
            min='0.0'
            onChange={(event) => { setBalance(event.target.value) }}
            placeholder='0.00'
            step='0.01'
            type='number'
            value={balance}
          />
          {required && <p className='text-red-500 text-xs italic'>Please fill out this field.</p>}
        </td>

        <td />

        <td className='px-6 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium'>
          <form
            onSubmit={handleSubmit}
          >
            <button
              className='bg-primary hover:bg-gray-800 text-white font-bold py-2
                         px-4 rounded focus:outline-none focus:shadow-outline'
              type='submit'
            >
              {snapshot.balanceCents ? 'Update' : 'Create'}
            </button>
          </form>
        </td>
      </tr>
    </>
  )
}

export default SnapshotForm
