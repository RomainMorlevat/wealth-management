import React, { useState } from 'react'

import SnapshotForm from '../snapshots/SnapshotForm'

const SnapshotsTable = ({ accountBalance, createSnapshot, editSnapshot, snapshots }) => {
  const [snapshotFormActive, setSnapshotFormActive] = useState(false)
  const [snapshotToEdit, setSnapshotToEdit] = useState()

  const handleFormDisplay = (snapshot) => {
    setSnapshotFormActive(!snapshotFormActive)
    setSnapshotToEdit(snapshot)
  }

  return (
    <div className='flex flex-col mt-2'>
      <div className='-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8'>
        <div className='align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200'>
          <table className='min-w-full'>
            <thead>
              <tr>
                <th className='px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider'>
                  Balance
                </th>
                <th className='px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider'>
                  Date
                </th>
                <th className='px-6 py-3 border-b border-gray-200 bg-gray-50 text-right'>
                  <button
                    className='bg-white border border-gray-300 duration-150 ease-in-out
                               focus:border-blue-300 focus:outline-none focus:shadow-outline
                               font-medium hover:bg-gray-50 inline-flex items-center
                               leading-5 px-4 py-2 rounded-md text-gray-700 text-sm
                               transition'
                    onClick={() => { handleFormDisplay() }}
                    type='button'
                  >
                    {snapshotFormActive ? 'Cancel' : 'New'}
                  </button>
                </th>
              </tr>
            </thead>
            <tbody className='bg-white'>
              {snapshotFormActive &&
                <SnapshotForm
                  disactivate={() => { setSnapshotFormActive(false) }}
                  httpAction={snapshotToEdit ? editSnapshot : createSnapshot}
                  snapshot={snapshotToEdit}
                />}
              {snapshots.map((snapshot) => (
                <tr key={snapshot.id}>
                  <td className='px-6 py-4 whitespace-no-wrap border-b border-gray-200'>
                    <div className='text-sm leading-5 text-gray-900'>
                      {(snapshot.balanceCents / 100)
                        .toLocaleString('fr-FR',
                          {
                            style: 'currency',
                            currency: snapshot.balanceCurrency
                          })}
                    </div>
                  </td>
                  <td className='px-6 py-4 whitespace-no-wrap border-b border-gray-200'>
                    <div className='text-sm leading-5 text-gray-900'>
                      {new Date(snapshot.createdOn).toDateString()}
                    </div>
                  </td>
                  <td className='px-6 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium'>
                    <button
                      className='text-indigo-600 hover:text-indigo-900 focus:outline-none focus:underline'
                      onClick={() => { handleFormDisplay(snapshot) }}
                      type='button'
                    >
                      {snapshotFormActive ? 'Cancel' : 'Edit'}
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default SnapshotsTable
