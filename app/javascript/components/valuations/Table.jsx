import React from 'react'

const ValuationsTable = (valuations) => (
  <table className='w-full mt-4 bg-white shadow-md rounded'>
    <thead>
      <tr className='bg-gray-200 text-gray-600 uppercase text-sm leading-normal'>
        <th className='py-3 px-6 text-left'>Date</th>
        <th className='py-3 px-6 text-left'>Price</th>
      </tr>
    </thead>
    <tbody className='text-gray-600 text-sm font-light'>
      {valuations.map((valuation, index) => (
        <tr key={index} className='border-b border-gray-200 hover:bg-gray-100'>
          <td className='py-3 px-6 text-left whitespace-no-wrap'>
            {new Date(valuation.date).toLocaleDateString()}
          </td>
          <td className='py-3 px-6 text-left'>
            {(valuation.price_cents / 100).toLocaleString('fr-FR', {
              style: 'currency',
              currency: 'EUR'
            })}
          </td>
        </tr>
      ))}
    </tbody>
  </table>
)

export default ValuationsTable
