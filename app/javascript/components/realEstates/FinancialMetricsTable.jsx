import React from 'react'

const FinancialMetricsTable = (realEstate) => {
  const netValue = realEstate.currentValuation - realEstate.totalAmountRemaining

  const formatCurrency = (amount) =>
    (amount / 100).toLocaleString('fr-FR', {
      style: 'currency',
      currency: 'EUR'
    })

  const metrics = [
    { name: 'Purchase Price', value: formatCurrency(realEstate.purchasePriceCents) },
    { name: 'Current Valuation', value: formatCurrency(realEstate.currentValuation) },
    { name: 'Net value', value: formatCurrency(netValue) }
  ]

  return (
    <table className='w-full mt-4 bg-white shadow-md rounded'>
      <thead>
        <tr className='bg-gray-200 text-gray-600 uppercase text-sm leading-normal'>
          <th className='py-3 px-6 text-left'>Metric</th>
          <th className='py-3 px-6 text-right'>Value</th>
        </tr>
      </thead>
      <tbody className='text-gray-600 text-sm font-light'>
        {metrics.map((metric, index) => (
          <tr key={index} className='border-b border-gray-200 hover:bg-gray-100'>
            <td className='py-3 px-6 text-left font-medium'>{metric.name}</td>
            <td className='py-3 px-6 text-right'>{metric.value}</td>
          </tr>
        ))}
      </tbody>
    </table>
  )
}

export default FinancialMetricsTable
