import React, { useState } from 'react'

const Form = ({ httpAction, realEstate = {} }) => {
  const [name, setName] = useState(realEstate.name || '')
  const [purchasePrice, setPurchasePrice] = useState(realEstate.purchasePriceCents / 100 || '')

  const handleName = (event) => {
    setName(event.target.value)
  }

  const handlePurchasePrice = (event) => {
    setPurchasePrice(event.target.value)
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    const payload = {
      name: name,
      purchase_price_cents: purchasePrice * 100
    }

    httpAction(payload)
  }

  return (
    <div className='w-full max-w-xs mx-auto w-3/5'>
      <form
        className='bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4'
        onSubmit={handleSubmit}
      >
        <input
          className='shadow appearance-none border rounded w-full py-2 px-3
                     text-gray-700 leading-tight mb-3
                     focus:outline-none focus:shadow-outline'
          id='name'
          onChange={handleName}
          placeholder='Name'
          type='text'
          value={name}
        />

        <input
          className='shadow appearance-none border rounded w-full py-2 px-3
                     text-gray-700 mb-3 leading-tight
                     focus:outline-none focus:shadow-outline'
          id='bankName'
          onChange={handlePurchasePrice}
          placeholder='Purchase price'
          type='text'
          value={purchasePrice}
        />

        <button
          className='bg-primary hover:bg-gray-800 text-white font-bold py-2
                     px-4 rounded focus:outline-none focus:shadow-outline'
          type='submit'
        >
          {realEstate.name ? 'Update' : 'Create'}
        </button>
      </form>
    </div>
  )
}

export default Form
