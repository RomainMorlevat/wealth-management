import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Accounts from '../pages/Accounts'
import Account from '../pages/Account'
import Dashboard from '../pages/Dashboard'
import RealEstates from '../pages/RealEstates'
import Navbar from './Navbar'
import Underlyings from '../pages/Underlyings'

export default props => {
  return (
    <>
      <Router>
        <Navbar />

        <div className='container md:px-4 mx-auto pt-3'>
          <Switch>
            <Route path='/' exact component={Dashboard} />
            <Route path='/dashboard' exact component={Dashboard} />
            <Route path='/accounts' exact component={Accounts} />
            <Route path='/account/:id' component={Account} />
            <Route path='/real_estates' exact component={RealEstates} />
            <Route path='/underlyings' component={Underlyings} />
          </Switch>
        </div>
      </Router>
    </>
  )
}
