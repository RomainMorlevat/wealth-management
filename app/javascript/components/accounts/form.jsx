import React, { useState } from 'react'

const Form = ({ httpAction, account = {} }) => {
  const [name, setName] = useState(account.name || '')
  const [bankName, setBankName] = useState(account.bankName || '')
  const [category, setCategory] = useState(account.category || '')

  const handleName = (event) => {
    setName(event.target.value)
  }

  const handleBankName = (event) => {
    setBankName(event.target.value)
  }

  const handleCategory = (event) => {
    setCategory(event.target.value)
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    const payload = {
      bank_name: bankName,
      category: category,
      name: name
    }

    httpAction(payload)
  }

  return (
    <div className='w-full max-w-xs mx-auto w-3/5'>
      <form
        className='bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4'
        onSubmit={handleSubmit}
      >
        <input
          className='shadow appearance-none border rounded w-full py-2 px-3
                     text-gray-700 leading-tight mb-3
                     focus:outline-none focus:shadow-outline'
          id='name'
          onChange={handleName}
          placeholder='Name'
          type='text'
          value={name}
        />

        <input
          className='shadow appearance-none border rounded w-full py-2 px-3
                     text-gray-700 mb-3 leading-tight
                     focus:outline-none focus:shadow-outline'
          id='bankName'
          onChange={handleBankName}
          placeholder='Bank Name'
          type='text'
          value={bankName}
        />

        <div className='relative'>
          <select
            className='block appearance-none w-full bg-gray-200 border
                       border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded
                       leading-tight focus:outline-none focus:bg-white
                       focus:border-gray-500 mb-3'
            id='grid-state'
            onChange={handleCategory}
            value={category}
          >
            <option value='' />
            <option value='current'>Current</option>
            <option value='saving'>Saving</option>
            <option value='stocks'>Stocks</option>
          </select>
          <div
            className='pointer-events-none absolute inset-y-0 right-0 flex
                       items-center px-2 text-gray-700'
          >
            <svg
              className='fill-current h-4 w-4'
              viewBox='0 0 20 20'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757
                   6.586 4.343 8z'
              />
            </svg>
          </div>
        </div>

        <button
          className='bg-primary hover:bg-gray-800 text-white font-bold py-2
                     px-4 rounded focus:outline-none focus:shadow-outline'
          type='submit'
        >
          {account.name ? 'Update' : 'Create'}
        </button>
      </form>
    </div>
  )
}

export default Form
