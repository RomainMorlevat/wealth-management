import React from 'react'
import { Link } from 'react-router-dom'

const Navbar = () => {
  const mainLinkClass = 'ml-4 px-3 py-2 rounded-md text-sm font-medium leading-5 text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700 transition duration-150 ease-in-out'

  return (
    <nav className='bg-gray-800'>
      <div className='max-w-7xl mx-auto px-2 sm:px-6 lg:px-8'>
        <div className='relative flex items-center justify-between h-16'>
          <div className='flex-1 flex items-center justify-center sm:items-stretch sm:justify-start'>
            <div className='sm:block sm:ml-6'>
              <div className='flex'>
                <Link to='/dashboard' className={mainLinkClass}>Dashboard</Link>
                <Link to='/accounts' className={mainLinkClass}>Accounts</Link>
                <Link to='/real_estates' className={mainLinkClass}>Real Estates</Link>
                <Link to='/underlyings' className={mainLinkClass}>Underlyings</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Navbar
