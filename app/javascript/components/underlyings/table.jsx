import React from 'react'

const UnderlyingsTable = ({ underlyings }) => {
  const rows = underlyings.map((underlying) => (
    <tr key={underlying.id}>
      <td className='border px-4 py-2'>{underlying.name}</td>
      <td className='border px-4 py-2'>{underlying.isin}</td>
      <td className='border px-4 py-2'>{underlying.ticker}</td>
      <td className='border px-4 py-2'>{underlying.kind}</td>
    </tr>
  ))

  const headerClass = 'px-4 py-3 text-xs font-medium text-gray-300 uppercase tracking-wider'

  return (
    <div className='flex flex-col mt-2'>
      <div className='-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8'>
        <div className='align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-300'>
          <table className='min-w-full'>
            <thead>
              <tr className='bg-primary text-left'>
                <th className={headerClass}>Name</th>
                <th className={headerClass}>ISIN</th>
                <th className={headerClass}>Ticker</th>
                <th className={headerClass}>Kind</th>
              </tr>
            </thead>
            <tbody>
              {rows}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default UnderlyingsTable
