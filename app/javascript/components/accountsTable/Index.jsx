import React from 'react'
import { Link } from 'react-router-dom'

const AccountsTable = ({ accounts }) => {
  const rows = accounts.map((account) => (
    <tr key={account.id}>
      <td className='border px-4 py-2'><Link to={`account/${account.id}`}>{account.name}</Link></td>
      <td className='border px-4 py-2'>{account.bankName}</td>
      <td className='border px-4 py-2 text-right'>
        {(account.balanceCents / 100)
          .toLocaleString('fr-FR',
            { style: 'currency', currency: 'EUR' })}
      </td>
    </tr>
  ))

  return (
    <div className='flex flex-col mt-2'>
      <div className='-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8'>
        <div className='align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200'>
          <table className='min-w-full'>
            <thead>
              <tr className='bg-primary text-left'>
                <th className='px-4 py-3 text-xs font-medium text-gray-200 uppercase tracking-wider'>Name</th>
                <th className='px-4 py-3 text-xs font-medium text-gray-200 uppercase tracking-wider'>Bank Name</th>
                <th className='px-4 py-3 text-xs font-medium text-gray-200 uppercase tracking-wider text-right'>Balance</th>
              </tr>
            </thead>
            <tbody>
              {rows}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default AccountsTable
