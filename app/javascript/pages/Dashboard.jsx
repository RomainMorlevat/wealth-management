import fetch from 'unfetch'
import React, { useEffect, useState } from 'react'
import { Doughnut, Line } from 'react-chartjs-2'

const Dashboard = () => {
  const [accountStats, setAccountStats] = useState({})
  const [balancesOverTime, setBalancesOverTime] = useState({ datasets: [] })
  const [totalBalance, setTotalBalance] = useState(0.0)
  const [totalBalanceOverTime, setTotalBalanceOverTime] = useState({ datasets: [] })
  const [totalWealth, setTotalWealth] = useState(0.0)
  const [wealthStats, setWealthStats] = useState({})
  const [ytd, setYtd] = useState([])

  const colors = [
    '#387490',
    '#89B374',
    '#01ACB6',
    '#af3e4d',
    '#a97c73',
    '#f9a23a',
    '#efe099',
    '#72c596'
  ]
  const accountsLabels = Object.keys(accountStats)
  const accountsValues = Object.values(accountStats)
  const wealthLabels = Object.keys(wealthStats)
  const wealthValues = Object.values(wealthStats)
  const balancesValues = balancesOverTime.datasets.map((data, index) => {
    return {
      backgroundColor: colors[index],
      borderColor: colors[index],
      data: data.data,
      fill: false,
      label: data.label,
      lineTension: 0
    }
  })

  const accountsData = {
    labels: accountsLabels,
    datasets: [
      {
        backgroundColor: colors,
        data: accountsValues
      }
    ]
  }
  const balancesData = {
    labels: balancesOverTime.labels,
    datasets: balancesValues
  }
  const wealthData = {
    labels: wealthLabels,
    datasets: [
      {
        backgroundColor: colors,
        data: wealthValues
      }
    ]
  }

  useEffect(() => {
    let mounted = true

    mounted && getDashboard()

    return () => {
      mounted = false
    }
  }, [])

  const getDashboard = () => {
    fetch('/api/v1/dashboard')
      .then((response) => {
        if (response.ok) {
          return response.json()
        }
        throw new Error('Network response was not OK.')
      })
      .then((dashboard) => {
        setAccountStats(dashboard.accountStats)
        setBalancesOverTime(dashboard.balancesOverTime)
        setTotalBalance(dashboard.totalBalance)
        setTotalBalanceOverTime(dashboard.totalBalanceOverTime)
        setTotalWealth(dashboard.totalWealth)
        setWealthStats(dashboard.wealthStats)
        setYtd(dashboard.ytd)
      })
  }

  return (
    <>
      <div className='flex flex-row flex-wrap justify-around items-center'>
        <div className='bg-white shadow overflow-hidden sm:rounded-lg m-4 px-4 py-5 border-b border-gray-200 sm:px-6'>
          Total wealth:{' '}
          <span className='text-lg leading-6 font-medium text-gray-900'>
            {totalWealth.toLocaleString('fr-FR', { style: 'currency', currency: 'EUR' })}
          </span>
        </div>
        <div className='bg-white shadow overflow-hidden sm:rounded-lg m-4 px-4 py-5 border-b border-gray-200 sm:px-6'>
          Total balance:{' '}
          <span className='text-lg leading-6 font-medium text-gray-900'>
            {totalBalance.toLocaleString('fr-FR', { style: 'currency', currency: 'EUR' })}
          </span>
        </div>
        <div className='bg-white text-center shadow overflow-hidden sm:rounded-lg m-4 px-4 py-5 border-b border-gray-200 sm:px-6 w-1/4'>
          YTD performance
          <ul>
            {ytd.map((ytdPerf) => {
              return (
                <li className='flex justify-between text-lg leading-6 font-medium text-gray-900' key={ytdPerf.year}>
                  <div>{ytdPerf.year}:</div>
                  <div>
                    {ytdPerf.ytd_percentage.toLocaleString('fr-FR', {
                      style: 'percent',
                      minimumFractionDigits: 2
                    })}
                  </div>
                </li>
              )
            })}
          </ul>
        </div>
      </div>
      <div className='bg-white shadow overflow-hidden sm:rounded-lg m-4 px-4 py-5 border-b border-gray-200 sm:px-6'>
        <h3 className='text-lg leading-6 font-medium text-gray-900'>
          History of Balances of Accounts
        </h3>
        <Line data={balancesData} />
      </div>
      <div className='bg-white shadow overflow-hidden sm:rounded-lg m-4 px-4 py-5 border-b border-gray-200 sm:px-6'>
        <h3 className='text-lg leading-6 font-medium text-gray-900'>Total balance over time</h3>
        <Line data={totalBalanceOverTime} />
      </div>
      <div className='flex flex-row flex-wrap justify-center'>
        <div className='bg-white shadow overflow-hidden sm:rounded-lg m-4 px-4 py-5 border-b border-gray-200 sm:px-6 md:w-2/5'>
          <h3 className='text-lg leading-6 font-medium text-gray-900'>Account stats</h3>
          <Doughnut data={accountsData} />
        </div>
        <div className='bg-white shadow overflow-hidden sm:rounded-lg m-4 px-4 py-5 border-b border-gray-200 sm:px-6 md:w-2/5'>
          <h3 className='text-lg leading-6 font-medium text-gray-900'>Wealth stats</h3>
          <Doughnut data={wealthData} />
        </div>
      </div>
    </>
  )
}

export default Dashboard
