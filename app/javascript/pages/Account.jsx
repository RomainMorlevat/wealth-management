import fetch from 'unfetch'
import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'

import Form from '../components/accounts/form'
import SnapshotsTable from '../components/snapshots/SnapshotsTable'

const Account = () => {
  const { id } = useParams()
  const [account, setAccount] = useState({ balanceCents: 0, balanceCurrency: 'EUR' })
  const [displayForm, setDisplayForm] = useState(false)
  const [snapshots, setSnapshots] = useState([])
  const url = `/api/v1/accounts/${id}`

  useEffect(() => {
    let mounted = true

    mounted && getAccount()

    return () => {
      mounted = false
    }
  }, [])

  const getAccount = () => {
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json()
        }
        throw new Error('Network response was not OK.')
      })
      .then(account => {
        setAccount(account)
        setSnapshots(account.snapshots)
      })
      .catch(() => this.props.history.push(`/account/${id}`))
  }

  const patchAccount = (payload) => {
    const token = document.querySelector('meta[name="csrf-token"]').content
    fetch(url, {
      method: 'PATCH',
      headers: {
        'X-CSRF-Token': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    })
      .then(response => {
        if (response.ok) {
          return response.json()
        }
      })
      .then(account => {
        setAccount(account)
        setDisplayForm(!displayForm)
      })
  }

  const createSnapshot = (payload) => {
    const token = document.querySelector('meta[name="csrf-token"]').content
    fetch(`${url}/snapshots`, {
      method: 'POST',
      headers: {
        'X-CSRF-Token': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    })
      .then(response => {
        if (response.ok) {
          return response.json()
        }
      })
      .then(snapshot => {
        getAccount()
      })
  }

  const editSnapshot = (payload) => {
    const token = document.querySelector('meta[name="csrf-token"]').content
    fetch(`/api/v1/snapshots/${payload.snapshot_id}`, {
      method: 'PATCH',
      headers: {
        'X-CSRF-Token': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    })
      .then(response => {
        if (response.ok) {
          return response.json()
        }
      })
      .then(snapshot => {
        getAccount()
      })
  }

  const handleFormDisplay = () => {
    setDisplayForm(!displayForm)
  }

  return (
    <>
      <div className='md:flex md:items-center md:justify-between bg-primary px-4 py-2 sm:rounded-lg'>
        <div className='flex-1 min-w-0'>
          <div className='flex flex-row justify-between'>
            <h2 className='flex items-center text-2xl font-bold text-gray-300 sm:text-3xl sm:leading-9 sm:truncate'>
              {account.name}
            </h2>

            <h2 className='text-3xl font-bold leading-7 text-gray-300 sm:text-4xl mt-5 mr-4'>
              {(account.balanceCents / 100)
                .toLocaleString('fr-FR',
                  { style: 'currency', currency: 'EUR' })}
            </h2>
          </div>

          <div className='mt-0 flex items-center text-sm text-gray-500 sm:mr-6 sm:mt-2'>
            {account.bankName}
          </div>
        </div>

        <div className='mt-5 flex md:mt-0 md:ml-4'>
          <span className='shadow-sm rounded-md'>
            <button
              className='bg-white border border-gray-300 duration-150 ease-in-out
                         focus:border-blue-300 focus:outline-none focus:shadow-outline
                         font-medium hover:bg-gray-50 inline-flex items-center
                         leading-5 px-4 py-2 rounded-md text-gray-700 text-sm
                         transition'
              onClick={handleFormDisplay}
              type='button'
            >
              {displayForm ? 'Cancel' : 'Edit'}
            </button>
          </span>

          <Link to='/accounts'>
            <span className='ml-3 shadow-sm rounded-md'>
              <button
                className='bg-gray-800 border border-transparent duration-150
                           ease-in-out focus:outline-none focus:shadow-outline
                           font-medium hover:bg-gray-800 inline-flex items-center
                           leading-5 px-4 py-2 rounded-md text-sm text-white transition'
                type='button'
              >
                Back
              </button>
            </span>
          </Link>
        </div>
      </div>

      {displayForm && <Form httpAction={patchAccount} account={account} />}

      <SnapshotsTable
        accountBalance={account.balanceCents / 100}
        createSnapshot={createSnapshot}
        editSnapshot={editSnapshot}
        snapshots={snapshots}
      />
    </>
  )
}

export default Account
