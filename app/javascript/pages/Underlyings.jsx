import fetch from 'unfetch'
import React, { useEffect, useState } from 'react'

import UnderlyingForm from '../components/underlyings/form'
import UnderlyingsTable from '../components/underlyings/table'

const Underlyings = () => {
  const [underlyings, setUnderlyings] = useState([])
  const [isForm, setIsForm] = useState(false)
  const url = '/api/v1/underlyings'

  useEffect(() => {
    let mounted = true

    mounted && fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json()
        }
        throw new Error('Network response was not ok.')
      })
      .then(response => setUnderlyings(response))
      .catch(() => this.props.history.push('/underlyings'))

    return () => {
      mounted = false
    }
  }, [])

  const postUnderlying = (payload) => {
    const token = document.querySelector('meta[name="csrf-token"]').content
    fetch(url, {
      method: 'POST',
      headers: {
        'X-CSRF-Token': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    })
      .then(response => {
        if (response.ok) {
          return response.json()
        }
      })
      .then(underlying => {
        setUnderlyings([...underlyings, underlying])
        setIsForm(false)
      })
  }

  const handleFormDisplay = () => {
    setIsForm(!isForm)
  }

  return (
    <>
      {isForm &&
        <UnderlyingForm httpAction={postUnderlying} />}
      {!isForm &&
        <UnderlyingsTable underlyings={underlyings} />}
      <div className='mx-auto w-3/5 text-center mt-3'>
        <button
          className='bg-gray-800 hover:bg-gray-800 text-white font-bold py-2
                     px-4 rounded focus:outline-none focus:shadow-outline
                     max-w-xs mx-auto w-3/5'
          onClick={handleFormDisplay}
          type='button'
        >
          {isForm ? 'Cancel' : 'Create a new underlying'}
        </button>
      </div>
    </>
  )
}

export default Underlyings
