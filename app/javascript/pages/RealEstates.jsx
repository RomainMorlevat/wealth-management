import React, { useEffect, useState } from 'react'
import fetch from 'unfetch'

import RealEstateForm from '../components/realEstates/Form'
import FinancialMetricsTable from '../components/realEstates/FinancialMetricsTable'
import MortgageForm from '../components/mortgages/Form'
import MortgagesTable from '../components/mortgages/Table'
import ValuationForm from '../components/valuations/Form'
import ValuationsTable from '../components/valuations/Table'

const RealEstates = () => {
  const [realEstates, setRealEstates] = useState([])
  const [realEstateToEdit, setRealEstateToEdit] = useState()
  const [displayForm, setDisplayForm] = useState(false)
  const [displayMortgageForm, setDisplayMortgageForm] = useState(false)
  const [displayValuationForm, setDisplayValuationForm] = useState(false)
  const [selectedRealEstateId, setSelectedRealEstateId] = useState(null)
  const url = '/api/v1/real_estates'

  useEffect(() => {
    let mounted = true

    mounted &&
      fetch(url)
        .then((response) => {
          if (response.ok) {
            return response.json()
          }
          throw new Error('Network response was not ok.')
        })
        .then((response) => setRealEstates(response))
        .catch(() => this.props.history.push('/'))

    return () => {
      mounted = false
    }
  }, [])

  const postRealEstate = (payload) => {
    const token = document.querySelector('meta[name="csrf-token"]').content
    fetch(url, {
      method: 'POST',
      headers: {
        'X-CSRF-Token': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    })
      .then((response) => {
        if (response.ok) {
          return response.json()
        }
      })
      .then((realEstate) => {
        setRealEstates([...realEstates, realEstate])
        setDisplayForm(false)
      })
  }

  const editRealEstate = (payload) => {
    const token = document.querySelector('meta[name="csrf-token"]').content
    fetch(`${url}/${realEstateToEdit.id}`, {
      method: 'PATCH',
      headers: {
        'X-CSRF-Token': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    })
      .then((response) => {
        if (response.ok) {
          return response.json()
        }
      })
      .then((updatedRealEstate) => {
        const purgedRealEstates = realEstates.filter(
          (realEstate) => realEstate.id !== updatedRealEstate.id
        )
        setRealEstates([...purgedRealEstates, updatedRealEstate])
        setRealEstateToEdit(undefined)
        setDisplayForm(false)
      })
  }

  const postMortgage = (payload) => {
    const token = document.querySelector('meta[name="csrf-token"]').content
    fetch(`${url}/${selectedRealEstateId}/mortgages`, {
      method: 'POST',
      headers: {
        'X-CSRF-Token': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    })
      .then((response) => {
        if (response.ok) {
          return response.json()
        }
      })
      .then((mortgage) => {
        const updatedRealEstates = realEstates.map((realEstate) => {
          if (realEstate.id === selectedRealEstateId) {
            return {
              ...realEstate,
              mortgages: [...(realEstate.mortgages || []), mortgage]
            }
          }
          return realEstate
        })
        setRealEstates(updatedRealEstates)
        setDisplayMortgageForm(false)
        setSelectedRealEstateId(null)
      })
  }

  const postValuation = (payload) => {
    const token = document.querySelector('meta[name="csrf-token"]').content
    fetch(`${url}/${payload.real_estate_id}/valuations`, {
      method: 'POST',
      headers: {
        'X-CSRF-Token': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    })
      .then((response) => {
        if (response.ok) {
          return response.json()
        }
      })
      .then((valuation) => {
        const updatedRealEstates = realEstates.map((realEstate) => {
          if (realEstate.id === payload.real_estate_id) {
            return {
              ...realEstate,
              valuations: [...(realEstate.valuations || []), valuation]
            }
          }
          return realEstate
        })
        setRealEstates(updatedRealEstates)
        setDisplayValuationForm(false)
        setSelectedRealEstateId(null)
      })
  }

  const handleMortgageFormDisplay = (realEstateId) => {
    setSelectedRealEstateId(realEstateId)
    setDisplayMortgageForm(!displayMortgageForm)
  }
  const handleValuationFormDisplay = (realEstateId) => {
    setSelectedRealEstateId(realEstateId)
    setDisplayValuationForm(!displayValuationForm)
  }
  const handleFormDisplay = () => {
    setDisplayForm(!displayForm)
  }

  return (
    <div className='container mx-auto px-4'>
      {displayForm && (
        <RealEstateForm
          httpAction={realEstateToEdit ? editRealEstate : postRealEstate}
          realEstate={realEstateToEdit}
        />
      )}
      {displayMortgageForm && (
        <MortgageForm httpAction={postMortgage} onCancel={() => setDisplayMortgageForm(false)} />
      )}
      {displayValuationForm && (
        <ValuationForm
          realEstateId={selectedRealEstateId}
          onSubmit={postValuation}
          onCancel={() => setDisplayValuationForm(false)}
        />
      )}
      {!displayForm && !displayMortgageForm && !displayValuationForm && (
        <>
          <div className='grid grid-cols-1 gap-6 mt-6'>
            {realEstates.map((realEstate) => (
              <div key={realEstate.id} className='bg-white shadow-lg rounded-lg p-6'>
                <div className='flex justify-between items-center mb-4'>
                  <h2 className='text-2xl font-bold text-gray-800'>{realEstate.name}</h2>
                </div>
                <div className='flex space-x-4 mb-4'>
                  <button
                    className='bg-gray-800 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline'
                    onClick={() => {
                      setRealEstateToEdit(realEstate)
                      handleFormDisplay()
                    }}
                  >
                    Edit
                  </button>
                  <button
                    className='bg-blue-500 hover:bg-blue-700 text-white font-bold ml-2 py-2 px-4 rounded focus:outline-none focus:shadow-outline'
                    onClick={() => handleMortgageFormDisplay(realEstate.id)}
                  >
                    Add Mortgage
                  </button>
                  <button
                    className='bg-green-500 hover:bg-green-700 text-white font-bold ml-2 py-2 px-4 rounded focus:outline-none focus:shadow-outline'
                    onClick={() => handleValuationFormDisplay(realEstate.id)}
                  >
                    Add Valuation
                  </button>
                </div>

                <div className='mt-6 pt-4'>
                  <h3 className='text-xl font-semibold text-gray-700 mb-2'>Financial Metrics:</h3>
                  {FinancialMetricsTable(realEstate)}
                </div>

                {realEstate.mortgages && realEstate.mortgages.length > 0 && (
                  <div className='mt-6 pt-4'>
                    <h3 className='text-xl font-semibold text-gray-700 mb-2'>Mortgages:</h3>
                    {MortgagesTable(realEstate)}
                  </div>
                )}

                {realEstate.valuations && realEstate.valuations.length > 0 && (
                  <div className='mt-6 pt-4'>
                    <h3 className='text-xl font-semibold text-gray-700 mb-2'>Valuations:</h3>
                    {ValuationsTable(realEstate.valuations)}
                  </div>
                )}
              </div>
            ))}
          </div>
          <div className='text-center mt-8'>
            <button
              className='bg-gray-800 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline'
              onClick={handleFormDisplay}
              type='button'
            >
              {displayForm ? 'Cancel' : 'Create a new real estate'}
            </button>
          </div>
        </>
      )}
    </div>
  )
}

export default RealEstates
