import fetch from 'unfetch'
import React, { useEffect, useState } from 'react'

import AccountForm from '../components/accounts/form'
import AccountsTable from '../components/accountsTable/Index'

const Accounts = (props) => {
  const [accounts, setAccounts] = useState([])
  const [isForm, setIsForm] = useState(false)
  const url = '/api/v1/accounts'

  useEffect(() => {
    let mounted = true

    mounted && fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json()
        }
        throw new Error('Network response was not ok.')
      })
      .then(response => setAccounts(response))
      .catch(() => this.props.history.push('/'))

    return () => {
      mounted = false
    }
  }, [])

  const postAccount = (payload) => {
    const token = document.querySelector('meta[name="csrf-token"]').content
    fetch(url, {
      method: 'POST',
      headers: {
        'X-CSRF-Token': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    })
      .then(response => {
        if (response.ok) {
          return response.json()
        }
      })
      .then(account => {
        setAccounts([...accounts, account])
        setIsForm(false)
      })
  }

  const handleFormDisplay = () => {
    setIsForm(!isForm)
  }

  return (
    <>
      {isForm &&
        <AccountForm httpAction={postAccount} />}
      {!isForm &&
        <AccountsTable accounts={accounts} />}
      <div className='mx-auto w-3/5 text-center mt-3'>
        <button
          className='bg-gray-800 hover:bg-gray-800 text-white font-bold py-2
                     px-4 rounded focus:outline-none focus:shadow-outline
                     max-w-xs mx-auto w-3/5'
          onClick={handleFormDisplay}
          type='button'
        >
          {isForm ? 'Cancel' : 'Create a new account'}
        </button>
      </div>
    </>
  )
}

export default Accounts
