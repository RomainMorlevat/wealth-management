module Api
  module V1
    class AccountsController < ApiController
      def index
        @accounts = Account.all.order(name: :asc)
      end

      def show
        @account = Account.includes(:snapshots)
          .order("snapshots.created_at DESC")
          .find(params[:id])
      end

      def create
        @account = Account.new(account_params)
        if @account.save
          render :show, status: :created
        else
          render json: {
            errors: @account.errors
          }, status: :unprocessable_entity
        end
      end

      def update
        @account = Account.includes(:snapshots).find(params[:id])
        if @account.update(account_params)
          render :show, status: :ok
        else
          render json: {
            errors: [
              {
                detail: @account.errors
              }
            ]
          }, status: :unprocessable_entity
        end
      end

      private

      def account_params
        params.require(:account).permit(:category, :bank_name, :name)
      end
    end
  end
end
