module Api
  module V1
    class RealEstatesController < ApiController
      def index
        @real_estates = RealEstate.all.includes(:mortgages, :valuations).order(name: :asc)
      end

      def create
        @real_estate = RealEstate.new(real_estate_params)
        if @real_estate.save
          render :show, status: :created
        else
          render json: {
            errors: @real_estate.errors
          }, status: :unprocessable_entity
        end
      end

      def update
        @real_estate = RealEstate.find(params[:id])
        if @real_estate.update(real_estate_params)
          render :show, status: :ok
        else
          render json: {
            errors: [
              {
                detail: @real_estate.errors
              }
            ]
          }, status: :unprocessable_entity
        end
      end

      private

      def real_estate_params
        params.require(:real_estate).permit(:name, :purchase_price_cents)
      end
    end
  end
end
