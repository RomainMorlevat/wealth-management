module Api
  module V1
    class SnapshotsController < ApiController
      before_action :find_account, only: %i[create]

      def create
        @snapshot = @account.snapshots.build(snapshot_params)
        render_snapshot(:created)
      end

      def update
        @snapshot = Snapshot.find(params[:id])
        @snapshot.assign_attributes(snapshot_params)
        render_snapshot(:ok)
      end

      private

      def snapshot_params
        params.require(:snapshot)
          .permit(:balance_cents, :balance_currency)
      end

      def find_account
        @account = Account.find(params[:account_id])
      end

      def render_snapshot(status)
        if @snapshot.save
          render :show, status: status
        else
          render json: {
            errors: @snapshot.errors
          }, status: :unprocessable_entity
        end
      end
    end
  end
end
