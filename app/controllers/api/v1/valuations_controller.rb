module Api
  module V1
    class ValuationsController < ApiController
      before_action :set_real_estate

      def create
        @valuation = @real_estate.valuations.build(valuation_params)
        if @valuation.save
          render :show, status: :created, formats: [:json]
        else
          render json: {
            errors: @valuation.errors
          }, status: :unprocessable_entity
        end
      end

      private

      def set_real_estate
        @real_estate = RealEstate.find(params[:real_estate_id])
      end

      def valuation_params
        params.require(:valuation).permit(:price_cents, :date)
      end
    end
  end
end
