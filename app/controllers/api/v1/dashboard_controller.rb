module Api
  module V1
    class DashboardController < ApiController
      def index
        @dashboard_presenter = DashboardPresenter.new
      end
    end
  end
end
