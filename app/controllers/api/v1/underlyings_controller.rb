module Api
  module V1
    class UnderlyingsController < ApiController
      def index
        @underlyings = Underlying.all.order(name: :asc)
      end

      def create
        @underlying = Underlying.new(underlying_params)
        if @underlying.save
          render :show, status: :created
        else
          render json: {}, status: :unprocessable_entity
        end
      end

      private

      def underlying_params
        params.require(:underlying).permit(:isin, :kind, :name, :ticker)
      end
    end
  end
end
