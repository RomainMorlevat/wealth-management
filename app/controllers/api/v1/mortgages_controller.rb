module Api
  module V1
    class MortgagesController < ApiController
      def create
        @mortgage = Mortgage.new(mortgage_params)
        if @mortgage.save
          render :show, status: :created
        else
          render json: {
            errors: @mortgage.errors
          }, status: :unprocessable_entity
        end
      end

      private

      def mortgage_params
        params.require(:mortgage).permit(
          :loan_amount_cents,
          :interest_rate,
          :term_years,
          :start_date,
          :end_date,
          :monthly_payment_cents
        ).merge(real_estate_id: params[:real_estate_id])
      end
    end
  end
end
