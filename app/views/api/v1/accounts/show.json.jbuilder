json.partial! "account", account: @account
json.snapshots @account.snapshots.each do |snapshot|
  json.partial! "api/v1/snapshots/snapshot", snapshot: snapshot
end
