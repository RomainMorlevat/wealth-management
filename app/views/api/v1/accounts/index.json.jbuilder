json.array! @accounts.each do |account|
  json.partial! "account", account: account
end
