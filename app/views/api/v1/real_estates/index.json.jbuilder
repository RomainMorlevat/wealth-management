json.array! @real_estates.each do |real_estate|
  json.partial! "real_estate", real_estate: real_estate
end
