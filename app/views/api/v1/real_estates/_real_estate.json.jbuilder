json.currentValuation real_estate.current_valuation
json.id real_estate.id
json.name real_estate.name
json.purchasePriceCents real_estate.purchase_price_cents
json.totalAmountRepaid real_estate.total_amount_repaid
json.totalAmountRemaining real_estate.total_amount_remaining
json.mortgages real_estate.mortgages do |mortgage|
  json.partial! "api/v1/mortgages/mortgage", mortgage: mortgage
  json.amountRepaid mortgage.amount_repaid
  json.amountRemaining mortgage.amount_remaining
end
json.valuations real_estate.valuations do |valuation|
  json.partial! "api/v1/valuations/valuation", valuation: valuation
end
