json.id mortgage.id
json.loanAmountCents mortgage.loan_amount_cents
json.interestRate mortgage.interest_rate.to_f
json.termYears mortgage.term_years
json.startDate mortgage.start_date
json.endDate mortgage.end_date
json.monthlyPaymentCents mortgage.monthly_payment_cents
