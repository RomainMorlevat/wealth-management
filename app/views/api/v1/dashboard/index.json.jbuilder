json.accountStats @dashboard_presenter.account_stats
json.balancesOverTime @dashboard_presenter.balances_of_accounts_over_time
json.totalBalance @dashboard_presenter.total_balance
json.totalBalanceOverTime @dashboard_presenter.total_balance_over_time
json.totalWealth @dashboard_presenter.total_wealth
json.wealthStats @dashboard_presenter.wealth_stats
json.ytd @dashboard_presenter.ytd
