Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :accounts, only: %i[create index show update] do
        resources :snapshots, only: %i[create]
      end
      resources :dashboard, only: %i[index]
      resources :real_estates, only: %i[create index update] do
        resources :mortgages, only: %i[create]
        resources :valuations, only: %i[create]
      end
      resources :snapshots, only: %i[update]
      resources :underlyings, only: %i[create index]
    end
  end

  get "accounts", to: "home#index"
  get "account/:id", to: "home#index"
  get "dashboard", to: "home#index"
  get "real_estates", to: "home#index"
  get "underlyings", to: "home#index"

  root "home#index"
end
