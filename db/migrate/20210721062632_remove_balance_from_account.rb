class RemoveBalanceFromAccount < ActiveRecord::Migration[6.0]
  def change
    remove_monetize :accounts, :balance
  end
end
