class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.monetize :amount
      t.integer :kind
      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end
