class AddUnderlying < ActiveRecord::Migration[6.0]
  def change
    create_table :underlyings do |t|
      t.integer :kind
      t.string :isin
      t.string :name, null: false
      t.string :ticker

      t.timestamps
    end

    change_table :transactions do |t|
      t.integer :quantity
      t.monetize :fees, currency: {present: false}
      t.monetize :unit_price, currency: {present: false}
      t.references :underlying, foreign_key: true
    end
  end
end
