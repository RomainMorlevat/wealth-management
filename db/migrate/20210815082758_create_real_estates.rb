class CreateRealEstates < ActiveRecord::Migration[6.1]
  def change
    create_table :real_estates do |t|
      t.string :name, null: false
      t.monetize :purchase_price, null: false

      t.timestamps
    end
  end
end
