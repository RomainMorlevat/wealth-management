class CreateSnapshots < ActiveRecord::Migration[6.0]
  def change
    create_table :snapshots do |t|
      t.monetize :balance

      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end
