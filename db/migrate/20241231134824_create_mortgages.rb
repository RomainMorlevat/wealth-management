class CreateMortgages < ActiveRecord::Migration[6.1]
  def change
    create_table :mortgages do |t|
      t.references :real_estate, null: false, foreign_key: true
      t.integer :loan_amount_cents, default: 0, null: false
      t.string :loan_amount_currency, default: "EUR", null: false
      t.decimal :interest_rate, precision: 5, scale: 3, null: false
      t.integer :term_years, null: false
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.integer :monthly_payment_cents, default: 0, null: false
      t.string :monthly_payment_currency, default: "EUR", null: false

      t.timestamps
    end
  end
end
