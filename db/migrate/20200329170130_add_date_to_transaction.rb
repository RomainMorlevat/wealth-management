class AddDateToTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :transactions, :date, :date, null: false, default: -> { "CURRENT_DATE" }
  end
end
