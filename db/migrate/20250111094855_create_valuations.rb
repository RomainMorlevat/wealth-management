class CreateValuations < ActiveRecord::Migration[6.1]
  def change
    create_table :valuations do |t|
      t.references :real_estate, null: false, foreign_key: true
      t.integer :price_cents, null: false
      t.date :date, null: false

      t.timestamps
    end

    add_index :valuations, [:real_estate_id, :date], unique: true
  end
end
