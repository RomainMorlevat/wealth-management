# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2025_01_11_094855) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "name"
    t.integer "category"
    t.string "bank_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "mortgages", force: :cascade do |t|
    t.bigint "real_estate_id", null: false
    t.integer "loan_amount_cents", default: 0, null: false
    t.string "loan_amount_currency", default: "EUR", null: false
    t.decimal "interest_rate", precision: 5, scale: 3, null: false
    t.integer "term_years", null: false
    t.date "start_date", null: false
    t.date "end_date", null: false
    t.integer "monthly_payment_cents", default: 0, null: false
    t.string "monthly_payment_currency", default: "EUR", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["real_estate_id"], name: "index_mortgages_on_real_estate_id"
  end

  create_table "real_estates", force: :cascade do |t|
    t.string "name", null: false
    t.integer "purchase_price_cents", default: 0, null: false
    t.string "purchase_price_currency", default: "EUR", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "snapshots", force: :cascade do |t|
    t.integer "balance_cents", default: 0, null: false
    t.string "balance_currency", default: "EUR", null: false
    t.bigint "account_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_snapshots_on_account_id"
  end

  create_table "underlyings", force: :cascade do |t|
    t.integer "kind"
    t.string "isin"
    t.string "name", null: false
    t.string "ticker"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "valuations", force: :cascade do |t|
    t.bigint "real_estate_id", null: false
    t.integer "price_cents", null: false
    t.date "date", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["real_estate_id", "date"], name: "index_valuations_on_real_estate_id_and_date", unique: true
    t.index ["real_estate_id"], name: "index_valuations_on_real_estate_id"
  end

  add_foreign_key "mortgages", "real_estates"
  add_foreign_key "snapshots", "accounts"
  add_foreign_key "valuations", "real_estates"
end
