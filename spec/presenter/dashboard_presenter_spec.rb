require "rails_helper"

RSpec.describe DashboardPresenter, type: :presenter do
  include ActiveSupport::Testing::TimeHelpers

  subject(:presenter) { described_class.new }

  let(:account_1) { Account.create(bank_name: "Bachmanity Capital", category: "stocks", name: "Account 1") }
  let(:snapshot_1) { Snapshot.create(balance_cents: 12345, account: account_1) }
  let(:account_2) { Account.create(bank_name: "Raviga", category: "current", name: "Account 2") }
  let(:snapshot_2) { Snapshot.create(balance_cents: 7800, account: account_2) }
  let(:account_3) { Account.create(bank_name: "Raviga", category: "saving", name: "Account 3") }
  let(:snapshot_3) { Snapshot.create(balance_cents: 54321, account: account_3) }
  let(:account_4) { Account.create(bank_name: "Raviga", category: "saving", name: "Account 4") }
  let(:snapshot_4) { Snapshot.create(balance_cents: 67890, account: account_4) }
  let(:snapshot_4_1) { Snapshot.create(balance_cents: 67890, account: account_4, created_at: Time.zone.now - 1.day) }
  let(:real_estate) { RealEstate.create(name: "Home", purchase_price: 123456) }
  let(:valuation) { real_estate.valuations.create(date: Date.parse("2025-01-01"), price: 123457) }

  describe "#account_stats" do
    subject { presenter.account_stats }

    context "when no Account" do
      let(:expected_response) do
        {
          "No Data" => 0
        }
      end

      it { is_expected.to eq(expected_response) }
    end

    context "when Account" do
      let(:expected_response) do
        {
          "current" => 5.48,
          "saving" => 85.85,
          "stocks" => 8.67
        }
      end

      before do
        snapshot_1
        snapshot_2
        snapshot_3
        snapshot_4
        snapshot_4_1
      end

      it { is_expected.to eq(expected_response) }
    end
  end

  describe "#balances_of_accounts_over_time" do
    subject { presenter.balances_of_accounts_over_time }

    let(:expected_response) do
      {
        "datasets" => [],
        "labels" => []
      }
    end

    context "when no Account" do
      it { is_expected.to eq(expected_response) }
    end

    context "when no snapshots" do
      before do
        account_1
      end

      it { is_expected.to eq(expected_response) }
    end

    context "when snapshots" do
      let(:today) { Date.parse("20210714") }
      let(:snapshot_1) do
        Snapshot.create(
          balance_cents: 12345,
          account: account_1,
          created_at: today
        )
      end
      let(:snapshot_2) do
        Snapshot.create(
          balance_cents: 543,
          account: account_1,
          created_at: today - 1.month
        )
      end
      let(:snapshot_3) do
        Snapshot.create(
          balance_cents: 4200,
          account: account_2,
          created_at: today - 13.days
        )
      end
      let(:snapshot_4) do
        Snapshot.create(
          balance_cents: 4200,
          account: account_2,
          created_at: today - 1.month
        )
      end
      let(:expected_response) do
        {
          "datasets" => [
            {
              "label" => "Account 1",
              "data" => [{x: "2021-06-14", y: 5.43}, {x: "2021-07-14", y: 123.45}]
            },
            {
              "label" => "Account 2",
              "data" => [{x: "2021-06-14", y: 42.0}, {x: "2021-07-01", y: 42.0}]
            }
          ],
          "labels" => ["2021-06-14", "2021-07-01", "2021-07-14"]
        }
      end

      before do
        snapshot_1
        snapshot_2
        snapshot_3
        snapshot_4
      end

      it { is_expected.to eq(expected_response) }
    end
  end

  describe "#total_balance" do
    subject { presenter.total_balance }

    context "when no Account" do
      it { is_expected.to eq(0) }
    end

    context "when accounts" do
      let(:snapshot1) { Snapshot.create(account: account_1, balance: 123.45) }
      let(:snapshot2) { Snapshot.create(account: account_1, balance: 123.45, created_at: Date.yesterday) }

      before do
        snapshot1
        snapshot2
      end

      it { is_expected.to eq(123.45) }
    end
  end

  describe "#total_balance_over_time" do
    subject { presenter.total_balance_over_time }

    let(:expected_response) do
      {
        datasets: [{borderColor: "#483254", data: [{x: "2024-01-16", y: nil}], fill: false, label: "Year 2024", tension: 0.1}],
        labels: ["2024-01-16"]
      }
    end

    before { travel_to(Time.zone.local(2024, 1, 16, 11, 11, 44)) }
    after { travel_back }

    context "when no Snapshot" do
      it { is_expected.to eq(expected_response) }
    end

    context "when Snapshot" do
      let(:snapshot1) { Snapshot.create(account: account_1, balance: 10000, created_at: Date.parse("20230115")) }
      let(:snapshot2) { Snapshot.create(account: account_1, balance: 15000, created_at: Date.parse("20230215")) }
      let(:snapshot3) { Snapshot.create(account: account_2, balance: 20000, created_at: Date.parse("20230115")) }
      let(:snapshot4) { Snapshot.create(account: account_2, balance: 25000, created_at: Date.parse("20230315")) }
      let(:snapshot5) { Snapshot.create(account: account_1, balance: 18000, created_at: Date.parse("20240115")) }
      let(:expected_response) do
        {
          labels: ["2023-01-01", "2023-02-01", "2023-03-01", "2023-04-01", "2023-05-01", "2023-06-01",
            "2023-07-01", "2023-08-01", "2023-09-01", "2023-10-01", "2023-11-01", "2023-12-01", "2024-01-01", "2024-01-16"],
          datasets: [
            {
              label: "Year 2023",
              data: [
                {x: "2023-01-01", y: 0},
                {x: "2023-02-01", y: 30000},
                {x: "2023-03-01", y: 35000},
                {x: "2023-04-01", y: 40000},
                {x: "2023-05-01", y: 40000},
                {x: "2023-06-01", y: 40000},
                {x: "2023-07-01", y: 40000},
                {x: "2023-08-01", y: 40000},
                {x: "2023-09-01", y: 40000},
                {x: "2023-10-01", y: 40000},
                {x: "2023-11-01", y: 40000},
                {x: "2023-12-01", y: 40000}
              ],
              fill: false,
              borderColor: "#" + Digest::MD5.hexdigest("2023.0")[0..5],
              tension: 0.1
            },
            {
              label: "Year 2024",
              data: [
                {x: "2024-01-01", y: 40000},
                {x: "2024-01-16", y: 43000}
              ],
              fill: false,
              borderColor: "#" + Digest::MD5.hexdigest("2024.0")[0..5],
              tension: 0.1
            }
          ]
        }
      end

      before do
        snapshot1
        snapshot2
        snapshot3
        snapshot4
        snapshot5
      end

      it { is_expected.to eq(expected_response) }
    end
  end

  describe "#total_wealth" do
    subject { presenter.total_wealth }

    before do
      valuation
      snapshot_1
    end

    it { is_expected.to eq(123_580.45) }
  end

  describe "#wealth_stats" do
    subject { presenter.wealth_stats }

    let(:expected_response) do
      {
        "current" => 0.06,
        "realEstate" => 99.4,
        "saving" => 0.44,
        "stocks" => 0.1
      }
    end

    before do
      snapshot_1
      snapshot_2
      snapshot_3
      real_estate
    end

    it { is_expected.to eq(expected_response) }
  end

  describe "#ytd" do
    let(:snapshot1) { Snapshot.create(account: account_1, balance_cents: 10000, created_at: Date.new(2023, 1, 1)) }
    let(:snapshot2) { Snapshot.create(account: account_1, balance_cents: 12000, created_at: Date.new(2023, 6, 1)) }
    let(:snapshot3) { Snapshot.create(account: account_2, balance_cents: 20000, created_at: Date.new(2023, 1, 1)) }
    let(:snapshot4) { Snapshot.create(account: account_2, balance_cents: 22000, created_at: Date.new(2023, 6, 1)) }
    let(:snapshot5) { Snapshot.create(account: account_1, balance_cents: 15000, created_at: Date.new(2024, 1, 1)) }
    let(:snapshot6) { Snapshot.create(account: account_2, balance_cents: 25000, created_at: Date.new(2024, 1, 1)) }

    before do
      snapshot1
      snapshot2
      snapshot3
      snapshot4
      snapshot5
      snapshot6
      travel_to Time.zone.local(2024, 6, 15)
    end
    after { travel_back }

    context "when there is data for more than one year" do
      let(:expected_result) do
        [
          {year: 2023, ytd_percentage: 0.1333},
          {year: 2024, ytd_percentage: 0.0}
        ]
      end

      before do
      end

      it "calculates YTD performance correctly" do
        expect(subject.ytd).to eq(expected_result)
      end
    end

    context "when the first month balance is zero" do
      let(:snapshot7) { Snapshot.create(account: account_1, balance_cents: 0, created_at: Date.new(2025, 1, 1)) }
      let(:snapshot8) { Snapshot.create(account: account_2, balance_cents: 0, created_at: Date.new(2025, 1, 1)) }
      let(:snapshot9) { Snapshot.create(account: account_1, balance_cents: 5000, created_at: Date.new(2025, 6, 1)) }
      let(:snapshot10) { Snapshot.create(account: account_2, balance_cents: 7000, created_at: Date.new(2025, 6, 1)) }

      before do
        snapshot7
        snapshot8
        snapshot9
        snapshot10
        travel_to Time.zone.local(2025, 6, 15)
      end
      after { travel_back }

      it "excludes the year with zero first month balance" do
        expect(subject.ytd.map { |r| r[:year] }).not_to include(2025)
      end
    end
  end
end
