require "rails_helper"

RSpec.describe Api::V1::MortgagesController, type: :controller do
  let(:parsed_response) { JSON.parse(response.body) }

  describe "POST #create" do
    let(:real_estate) do
      RealEstate.create(name: "Home", purchase_price_cents: 100_000)
    end

    before do
      post :create, params: params, format: :json
    end

    context "when valid params" do
      let(:params) do
        {
          real_estate_id: real_estate.id,
          mortgage: {
            loan_amount_cents: 100_000,
            interest_rate: 3.14,
            term_years: 20,
            start_date: Date.parse("2024-01-01"),
            end_date: Date.parse("2044-01-01"),
            monthly_payment_cents: 562

          }
        }
      end
      let(:expected_response) do
        {
          "id" => Integer,
          "loanAmountCents" => 100_000,
          "interestRate" => 3.14,
          "termYears" => 20,
          "startDate" => "2024-01-01",
          "endDate" => "2044-01-01",
          "monthlyPaymentCents" => 562
        }
      end

      it "returns a 201 created" do
        expect(response).to have_http_status(:created)
      end

      it "returns a JSON" do
        expect(parsed_response).to match(expected_response)
      end
    end

    context "when invalid params" do
      let(:params) do
        {real_estate_id: real_estate.id, mortgage: {loan_amount_cents: nil}}
      end

      it "returns a 422 unprocessable_entity" do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end
