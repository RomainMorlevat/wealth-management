require "rails_helper"

RSpec.describe Api::V1::DashboardController, type: :controller do
  let(:parsed_response) { JSON.parse(response.body) }

  describe "GET #index" do
    let(:account_1) do
      Account.create(bank_name: "Bank",
        category: "current",
        name: "Account 1")
    end
    let(:account_2) do
      Account.create(bank_name: "Bank",
        category: "stocks",
        name: "Account 2")
    end
    let(:snapshot_1) do
      Snapshot.create(
        balance: 12345,
        created_at: Date.parse("20210814"),
        account: account_1
      )
    end
    let(:real_estate) { RealEstate.create(name: "Home", purchase_price: 123456) }

    let(:expected_response) do
      {
        "balancesOverTime" => {
          "labels" => ["2021-08-14"],
          "datasets" => [
            {
              "label" => "Account 1",
              "data" => [{"x" => "2021-08-14", "y" => 12345.0}]
            },
            {
              "label" => "Account 2",
              "data" => []
            }
          ]
        },
        "accountStats" => {
          "current" => 100.0,
          "stocks" => 0.0
        },
        "totalBalance" => 12345.0,
        "totalBalanceOverTime" => {
          "labels" => ["2021-08"],
          "datasets" => [
            {
              "label" => "Total balance",
              "data" => [{"x" => "2021-08", "y" => 12345.0}]
            }
          ]
        },
        "totalWealth" => 135_801.0,
        "wealthStats" => {
          "current" => 9.09,
          "realEstate" => 90.91,
          "stocks" => 0.0
        }
      }
    end

    before do
      snapshot_1
      account_2
      real_estate

      get :index, params: {}, format: :json
    end

    it "returns a 200 OK" do
      expect(response).to have_http_status(:ok)
    end

    it "returns a JSON with accounts" do
      expect(parsed_response).to match(expected_response)
    end
  end
end
