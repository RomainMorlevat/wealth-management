require "rails_helper"

RSpec.describe Api::V1::SnapshotsController, type: :controller do
  include ActiveSupport::Testing::TimeHelpers

  let(:parsed_response) { JSON.parse(response.body) }
  let(:account) do
    Account.create(bank_name: "Bank",
      category: "current",
      name: "Account")
  end

  before { travel_to(Time.zone.local(2021, 7, 14, 16, 35, 0)) }

  after { travel_back }

  describe "POST #create" do
    before do
      post :create, params: params, format: :json
    end

    context "when valid params" do
      let(:params) do
        {
          account_id: account.id,
          snapshot: {
            account_id: account.id,
            balance_cents: "12345"
          }
        }
      end
      let(:expected_response) do
        {
          "balanceCents" => 12345,
          "balanceCurrency" => "EUR",
          "createdOn" => "2021-07-14",
          "id" => Integer
        }
      end

      it "returns a 201 created" do
        expect(response).to have_http_status(:created)
      end

      it "returns a JSON with the new snapshot" do
        expect(parsed_response).to match(expected_response)
      end
    end

    context "when invalid params" do
      let(:params) { {account_id: account.id, snapshot: {balance_cents: nil}} }

      it "returns a 422 unprocessable_entity" do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "PATCH #update" do
    let(:snapshot) do
      Snapshot.create(account: account, balance: 123.45)
    end

    before do
      snapshot
      patch :update, params: params, format: :json
    end

    context "when valid params and simple snapshot" do
      let(:params) do
        {
          id: snapshot.id,
          snapshot: {
            balance_cents: "54321"
          }
        }
      end
      let(:expected_response) do
        {
          "balanceCents" => 54321,
          "balanceCurrency" => "EUR",
          "createdOn" => "2021-07-14",
          "id" => Integer
        }
      end

      it "returns a 200 ok" do
        expect(response).to have_http_status(:ok)
      end

      it "returns a JSON with the updated snapshot" do
        expect(parsed_response).to match(expected_response)
      end
    end

    context "when invalid params" do
      let(:params) { {id: snapshot.id, snapshot: {balance_cents: nil}} }

      it "returns a 422 unprocessable_entity" do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end
