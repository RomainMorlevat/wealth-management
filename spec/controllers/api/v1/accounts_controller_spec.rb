require "rails_helper"

RSpec.describe Api::V1::AccountsController, type: :controller do
  let(:parsed_response) { JSON.parse(response.body) }

  describe "GET #index" do
    let(:account_1) do
      Account.create(bank_name: "Bank",
        category: "current",
        name: "Account 1")
    end
    let(:account_2) do
      Account.create(bank_name: "Bank",
        category: "stocks",
        name: "Account 2")
    end
    let(:snapshot_1) { Snapshot.create(balance_cents: 12_345, account: account_1) }
    let(:snapshot_2) { Snapshot.create(balance_cents: 9_876, account: account_2) }

    let(:expected_response) do
      [
        {
          "balanceCents" => 12_345,
          "bankName" => "Bank",
          "category" => "current",
          "id" => account_1.id,
          "name" => "Account 1"
        },
        {
          "balanceCents" => 9_876,
          "bankName" => "Bank",
          "category" => "stocks",
          "id" => account_2.id,
          "name" => "Account 2"
        }
      ]
    end

    before do
      snapshot_1
      snapshot_2
      get :index, params: {}, format: :json
    end

    it "returns a 200 OK" do
      expect(response).to have_http_status(:ok)
    end

    it "returns a JSON with accounts" do
      expect(parsed_response).to match(expected_response)
    end
  end

  describe "GET #show" do
    let(:account) do
      Account.create(bank_name: "Bank",
        category: "current",
        name: "Account 1")
    end
    let(:snapshot_1) do
      Snapshot.create(balance: 123.45, account: account, created_at: Date.parse("20210714"))
    end
    let(:snapshot_2) do
      Snapshot.create(balance: 321.45, account: account, created_at: Date.parse("20210713"))
    end

    context "when id is found" do
      let(:expected_response) do
        {
          "balanceCents" => 12345,
          "bankName" => "Bank",
          "category" => "current",
          "id" => account.id,
          "name" => "Account 1",
          "snapshots" => [
            {
              "balanceCents" => 12345,
              "balanceCurrency" => "EUR",
              "createdOn" => "2021-07-14",
              "id" => snapshot_1.id
            },
            {
              "balanceCents" => 32145,
              "balanceCurrency" => "EUR",
              "createdOn" => "2021-07-13",
              "id" => snapshot_2.id
            }
          ]
        }
      end

      before do
        snapshot_1
        snapshot_2
        get :show, params: {id: account.id}, format: :json
      end

      it "returns a 200 OK" do
        expect(response).to have_http_status(:ok)
      end

      it "returns a JSON with accounts" do
        expect(parsed_response).to match(expected_response)
      end
    end

    context "when id is not found" do
      before do
        get :show, params: {id: 0}, format: :json
      end

      it "returns a 404 not_found" do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe "POST #create" do
    before do
      post :create, params: params, format: :json
    end

    context "when valid params" do
      let(:params) do
        {
          account: {
            bank_name: "Bachmanity Capital",
            category: "stocks",
            name: "Account"
          }
        }
      end
      let(:expected_response) do
        {
          "balanceCents" => 0,
          "bankName" => "Bachmanity Capital",
          "category" => "stocks",
          "id" => Integer,
          "name" => "Account",
          "snapshots" => []
        }
      end

      it "returns a 201 created" do
        expect(response).to have_http_status(:created)
      end

      it "returns a JSON" do
        expect(parsed_response).to match(expected_response)
      end
    end

    context "when invalid params" do
      let(:params) { {account: {name: nil}} }

      it "returns a 422 unprocessable_entity" do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "PATCH #update" do
    let(:account) do
      Account.create(bank_name: "Bank",
        category: "current",
        name: "Account 1")
    end

    before do
      patch :update, params: params, format: :json
    end

    context "when valid params" do
      let(:params) do
        {
          id: account.id,
          account: {
            bank_name: "Bachmanity Capital",
            category: "stocks",
            name: "Account"
          }
        }
      end
      let(:expected_response) do
        {
          "balanceCents" => 0,
          "bankName" => "Bachmanity Capital",
          "category" => "stocks",
          "id" => Integer,
          "name" => "Account",
          "snapshots" => []
        }
      end

      it "returns a 200 OK" do
        expect(response).to have_http_status(:ok)
      end

      it "returns a JSON with the updated account" do
        expect(parsed_response).to match(expected_response)
      end
    end

    context "when invalid params" do
      let(:params) { {id: account.id, account: {name: nil}} }

      it "returns a 422 unprocessable_entity" do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end
