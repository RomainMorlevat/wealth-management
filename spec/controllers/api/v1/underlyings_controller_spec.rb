require "rails_helper"

RSpec.describe Api::V1::UnderlyingsController, type: :controller do
  let(:parsed_response) { JSON.parse(response.body) }

  describe "GET #index" do
    let(:underlying) do
      Underlying.create(isin: "US12345678",
        kind: nil,
        name: "Bachmanity Capital",
        ticker: "BC")
    end
    let(:expected_response) do
      [
        {
          "id" => Integer,
          "isin" => "US12345678",
          "kind" => nil,
          "name" => "Bachmanity Capital",
          "ticker" => "BC"
        }
      ]
    end

    before do
      underlying
      get :index, params: {}, format: :json
    end

    it "returns a 200 OK" do
      expect(response).to have_http_status(:ok)
    end

    it "returns a JSON with underlyings" do
      expect(parsed_response).to match(expected_response)
    end
  end

  describe "POST #create" do
    before do
      post :create, params: params, format: :json
    end

    context "when valid params" do
      let(:params) do
        {
          underlying: {
            isin: "US12345678",
            kind: "stocks",
            name: "Bachmanity Capital",
            ticker: "BC"
          }
        }
      end
      let(:expected_response) do
        {
          "id" => Integer,
          "isin" => "US12345678",
          "kind" => "stocks",
          "name" => "Bachmanity Capital",
          "ticker" => "BC"
        }
      end

      it "returns a 201 created" do
        expect(response).to have_http_status(:created)
      end

      it "returns a JSON with the new underlying" do
        expect(parsed_response).to match(expected_response)
      end
    end

    context "when invalid params" do
      let(:params) { {underlying: {name: nil}} }

      it "returns a 422 unprocessable_entity" do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end
