require "rails_helper"

RSpec.describe Api::V1::ValuationsController, type: :controller do
  let(:real_estate) { RealEstate.create(name: "Test Property", purchase_price_cents: 10000000) }
  let(:parsed_response) { JSON.parse(response.body) }

  before do
    real_estate
  end

  describe "POST #create" do
    before do
      post :create, params: {real_estate_id: real_estate.id, valuation: params}
    end

    context "with valid parameters" do
      let(:params) { {price_cents: 15000000, date: Date.parse("2025-01-12")} }
      let(:expected_response) do
        {
          "date" => "2025-01-12",
          "id" => Integer,
          "price_cents" => 150_000_00
        }
      end

      it "renders the :show template" do
        expect(parsed_response).to match(expected_response)
      end

      it "responds with created status" do
        expect(response).to have_http_status(:created)
      end
    end

    context "with invalid parameters" do
      let(:params) { {price: "", date: ""} }

      it "does not create a new Valuation" do
        expect { post :create, params: {real_estate_id: real_estate.id, valuation: params} }.not_to change { real_estate.valuations.count }
      end

      it "renders a JSON response with errors" do
        expect(response).to have_http_status(:unprocessable_entity)
        expect(JSON.parse(response.body)).to eq({
          "errors" => {
            "price" => ["is not a number"],
            "date" => ["can't be blank"],
            "price_cents" => ["can't be blank", "is not a number"]
          }
        })
      end
    end
  end
end
