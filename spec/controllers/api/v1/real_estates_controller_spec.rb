require "rails_helper"

RSpec.describe Api::V1::RealEstatesController, type: :controller do
  let(:parsed_response) { JSON.parse(response.body) }

  describe "GET #index" do
    let(:real_estate) do
      RealEstate.create(name: "Home", purchase_price_cents: 100)
    end
    let(:mortgage) do
      real_estate.mortgages.create(
        loan_amount_cents: 80,
        interest_rate: 3.5,
        term_years: 30,
        start_date: Time.zone.today,
        end_date: 30.years.from_now,
        monthly_payment_cents: 359
      )
    end
    let(:valuation) do
      real_estate.valuations.create(
        date: Date.parse("2025-01-12"),
        price_cents: 101
      )
    end
    let(:expected_response) do
      [
        {
          "currentValuation" => 101,
          "id" => real_estate.id,
          "name" => "Home",
          "purchasePriceCents" => 100,
          "totalAmountRepaid" => 0,
          "totalAmountRemaining" => 80,
          "mortgages" => [
            {
              "id" => mortgage.id,
              "loanAmountCents" => 80,
              "interestRate" => 3.5,
              "termYears" => 30,
              "startDate" => mortgage.start_date.as_json,
              "endDate" => mortgage.end_date.as_json,
              "monthlyPaymentCents" => 359,
              "amountRepaid" => 0,
              "amountRemaining" => 80
            }
          ],
          "valuations" => [
            {
              "id" => valuation.id,
              "date" => "2025-01-12",
              "price_cents" => 101
            }
          ]
        }
      ]
    end

    before do
      real_estate
      mortgage
      valuation
      get :index, params: {}, format: :json
    end

    it "returns a 200 OK" do
      expect(response).to have_http_status(:ok)
    end

    it "returns a JSON with real_estates" do
      expect(parsed_response).to match(expected_response)
    end
  end

  describe "POST #create" do
    before do
      post :create, params: params, format: :json
    end

    context "when valid params" do
      let(:params) do
        {
          real_estate: {
            name: "Home",
            purchase_price_cents: 1234500
          }
        }
      end
      let(:expected_response) do
        {
          "currentValuation" => nil,
          "id" => Integer,
          "name" => "Home",
          "purchasePriceCents" => 1234500,
          "totalAmountRepaid" => 0,
          "totalAmountRemaining" => 0,
          "mortgages" => [],
          "valuations" => []
        }
      end

      it "returns a 201 created" do
        expect(response).to have_http_status(:created)
      end

      it "returns a JSON" do
        expect(parsed_response).to match(expected_response)
      end
    end

    context "when invalid params" do
      let(:params) { {real_estate: {name: nil}} }

      it "returns a 422 unprocessable_entity" do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "PATCH #update" do
    let(:real_estate) { RealEstate.create(name: "Home", purchase_price: 12345) }

    before do
      patch :update, params: params, format: :json
    end

    context "when valid params" do
      let(:params) do
        {
          id: real_estate.id,
          real_estate: {
            name: "Home",
            purchase_price_cents: 1234567
          }
        }
      end
      let(:expected_response) do
        {
          "currentValuation" => nil,
          "id" => Integer,
          "name" => "Home",
          "purchasePriceCents" => 1234567,
          "totalAmountRepaid" => 0,
          "totalAmountRemaining" => 0,
          "mortgages" => [],
          "valuations" => []
        }
      end

      it "returns a 200 OK" do
        expect(response).to have_http_status(:ok)
      end

      it "returns a JSON with the updated real_estate" do
        expect(parsed_response).to match(expected_response)
      end
    end

    context "when invalid params" do
      let(:params) { {id: real_estate.id, real_estate: {name: nil}} }

      it "returns a 422 unprocessable_entity" do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end
