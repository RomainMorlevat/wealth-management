require "rails_helper"

RSpec.describe Underlying, type: :model do
  describe "validations" do
    subject { Underlying.create(name: "Bachmanity Capital") }

    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:isin) }
  end

  describe "enums" do
    it do
      should define_enum_for(:kind)
        .with_values(stocks: 0, bonds: 1)
    end
  end
end
