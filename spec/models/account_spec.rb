require "money-rails/test_helpers"
require "rails_helper"

RSpec.describe Account, type: :model do
  describe "associations" do
    it { should have_many(:snapshots).dependent(:destroy) }
  end

  describe "validations" do
    it { should validate_presence_of(:bank_name) }
    it { should validate_presence_of(:category) }
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name).scoped_to(:bank_name) }
  end

  describe "enums" do
    it do
      should define_enum_for(:category)
        .with_values(current: 0, saving: 1, stocks: 2)
    end
  end

  describe "#balance_cents" do
    let(:subject) { account.balance_cents }

    let(:account) do
      Account.create(
        bank_name: "Bachmanity Capital",
        category: "stocks",
        name: "Account 1"
      )
    end
    let(:snapshot_1) do
      Snapshot.create(
        balance_cents: 12345,
        account: account,
        created_at: today
      )
    end
    let(:snapshot_2) do
      Snapshot.create(
        balance_cents: 543,
        account: account,
        created_at: today - 1.month
      )
    end
    let(:today) { Date.parse("20210714") }

    before { account }

    context "when no snapshots" do
      it { is_expected.to eq(0) }
    end

    context "when snapshots" do
      before do
        snapshot_1
        snapshot_2
      end

      it { is_expected.to eq(12345) }
    end
  end
end
