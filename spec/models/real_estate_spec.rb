require "money-rails/test_helpers"
require "rails_helper"

RSpec.describe RealEstate, type: :model do
  let(:real_estate) { RealEstate.create(name: "Test", purchase_price: 117_500) }
  let(:start_date) { 1.year.ago.to_date }
  let(:mortgage_1) do
    Mortgage.create(
      real_estate: real_estate,
      loan_amount_cents: 1_000_000_00,
      monthly_payment_cents: 4_490_00,
      start_date: start_date,
      end_date: start_date + 30.years,
      interest_rate: 3.5,
      term_years: 30
    )
  end
  let(:mortgage_2) do
    Mortgage.create(
      real_estate: real_estate,
      loan_amount_cents: 500_000_00,
      monthly_payment_cents: 2_245_00,
      start_date: start_date,
      end_date: start_date + 30.years,
      interest_rate: 3.5,
      term_years: 30
    )
  end

  describe "associations" do
    it { should have_many(:mortgages) }
    it { should have_many(:valuations) }
  end

  describe "validations" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:purchase_price_cents) }
    it { is_expected.to monetize(:purchase_price) }
  end

  describe "#total_amount_repaid" do
    before do
      mortgage_1
      mortgage_2
    end

    it "calculates the total amount repaid across all mortgages" do
      expected_total = (4_490_00 + 2_245_00) * 12
      expect(real_estate.total_amount_repaid).to eq(expected_total)
    end
  end

  describe "#total_amount_remaining" do
    before do
      mortgage_1
      mortgage_2
    end

    it "calculates the total amount remaining across all mortgages" do
      expected_total = (1_000_000_00 + 500_000_00) - ((4_490_00 + 2_245_00) * 12)
      expect(real_estate.total_amount_remaining).to eq(expected_total)
    end
  end

  describe "#current_valuation" do
    subject { real_estate.current_valuation }

    let(:real_estate) { RealEstate.create(name: "Test", purchase_price: 117500) }

    context "with valuations" do
      let(:first_valuation) { Valuation.create(date: 1.month.ago, price: 65_000, real_estate: real_estate) }
      let(:last_valuation) { Valuation.create(date: Date.current, price: 98_000, real_estate: real_estate) }

      before do
        last_valuation
        first_valuation
      end

      it { is_expected.to eq(last_valuation.price_cents) }
    end

    context "with no valuation" do
      before { real_estate }

      it { is_expected.to eq(11750000) }
    end
  end
end
