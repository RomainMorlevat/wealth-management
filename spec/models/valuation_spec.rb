require "rails_helper"
require "money-rails/test_helpers"

RSpec.describe Valuation, type: :model do
  describe "#associations" do
    it { should belong_to(:real_estate) }
  end

  describe "validations" do
    it { should validate_presence_of(:price_cents) }
    it { should validate_presence_of(:date) }
  end

  it { is_expected.to monetize(:price_cents) }

  describe "uniqueness validation" do
    subject { Valuation.new(date: Date.current, price_cents: 117_500, real_estate: real_estate) }

    let(:real_estate) { RealEstate.create(name: "Test", purchase_price: 117_500) }

    it { should validate_uniqueness_of(:date).scoped_to(:real_estate_id) }
  end
end
