require "money-rails/test_helpers"
require "rails_helper"

RSpec.describe Mortgage, type: :model do
  let(:real_estate) { RealEstate.create(name: "Test", purchase_price: 117_500) }

  subject(:mortgage) do
    described_class.new(
      real_estate: real_estate,
      loan_amount_cents: 1_000_000_00,
      loan_amount_currency: "EUR",
      interest_rate: interest_rate,
      term_years: 30,
      start_date: start_date,
      end_date: end_date,
      monthly_payment_cents: 4_490_00,
      monthly_payment_currency: "EUR"
    )
  end
  let(:end_date) { 30.years.from_now.to_date }
  let(:interest_rate) { 3.14 }
  let(:start_date) { Time.zone.today }

  describe "associations" do
    it { should belong_to(:real_estate) }
  end

  describe "validations" do
    it { should validate_presence_of(:loan_amount_cents) }
    it { should validate_numericality_of(:loan_amount_cents).is_greater_than_or_equal_to(0) }
    it { should validate_presence_of(:loan_amount_currency) }
    it { should validate_presence_of(:interest_rate) }
    it { should validate_numericality_of(:interest_rate).is_greater_than_or_equal_to(0).is_less_than(100) }
    it { should validate_presence_of(:term_years) }
    it { should validate_numericality_of(:term_years).is_greater_than(0) }
    it { should validate_presence_of(:start_date) }
    it { should validate_presence_of(:end_date) }
    it { should validate_presence_of(:monthly_payment_cents) }
    it { should validate_numericality_of(:monthly_payment_cents).is_greater_than_or_equal_to(0) }
    it { should validate_presence_of(:monthly_payment_currency) }
    it { is_expected.to monetize(:loan_amount) }
    it { is_expected.to monetize(:monthly_payment) }
  end

  describe "custom validations" do
    context "when end_date is after start_date" do
      it "is valid" do
        expect(mortgage).to be_valid
      end
    end

    context "when end_date is before start_date" do
      let(:end_date) { start_date - 1.day }

      it "is not valid when end_date is equal to start_date" do
        expect(mortgage).to_not be_valid
        expect(mortgage.errors[:end_date]).to include("must be after the start date")
      end
    end
  end

  describe "#amount_repaid" do
    context "with start date in the past" do
      let(:start_date) { 1.year.ago.to_date }

      it "calculates the correct amount repaid" do
        expect(mortgage.amount_repaid).to eq(5388000)
      end
    end

    context "with start date in the futur" do
      let(:start_date) { 1.month.from_now.to_date }

      it "returns zero" do
        expect(mortgage.amount_repaid).to eq(0)
      end
    end
  end

  describe "#amount_remaining" do
    context "with start date in the past" do
      let(:start_date) { 1.year.ago.to_date }

      it "calculates the correct amount remaining" do
        expected_remaining = mortgage.loan_amount_cents - (mortgage.monthly_payment_cents * 12)
        expect(mortgage.amount_remaining).to eq(expected_remaining)
      end
    end

    context "with start date in the futur" do
      let(:start_date) { 1.month.from_now.to_date }

      it "equals the loan amount" do
        expect(mortgage.amount_remaining).to eq(mortgage.loan_amount_cents)
      end
    end
  end
end
