require "money-rails/test_helpers"
require "rails_helper"

RSpec.describe Snapshot, type: :model do
  describe "associations" do
    it { should belong_to(:account) }
  end

  describe "validations" do
    it { should validate_presence_of(:balance_cents) }
    it { is_expected.to monetize(:balance) }
  end

  describe "#created_on" do
    subject { snapshot.created_on }

    let(:snapshot) { Snapshot.new(created_at: Time.zone.parse("20210714")) }

    it { is_expected.to eq("2021-07-14") }
  end
end
