Ruby: [![Ruby Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://github.com/testdouble/standard)
JavaScript: [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/0051f9403f0142339f0b021662aff58a)](https://www.codacy.com/manual/RomainMorlevat/wealth-management?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=RomainMorlevat/wealth-management&amp;utm_campaign=Badge_Grade)

# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

*   Ruby version
2.7.0
To avoid warning messages in logs, prefix rails commands with `RUBYOPT='-W:no-deprecated -W:no-experimental'`.

*   System dependencies

*   Configuration
Run `bundle install` and it would be OK.

*   Database creation
`rails db:create`

*   Database initialization
`rails db:migrate`

*   How to run the test suite
`rspec`
You can add `--seed SEED_NUMBER` to run tests in a particular order.

*   Services (job queues, cache servers, search engines, etc.)

*   Deployment instructions

## Plugins

*   Datepicker is from <https://reactdatepicker.com/>

## Development

Run `bin/webpack-dev-server` and `rails s`.
You may need to set `export NODE_OPTIONS=--openssl-legacy-provider` if webpack throw an error.
